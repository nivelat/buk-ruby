# Buk Ruby Client

Cliente para la API de Buk. https://help.buk.cl/es/articles/3641762-como-integrarse-a-buk-a-traves-de-la-api

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'buk-ruby', require: false
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install buk-ruby

## Usage

### Configure your client

Configure the client, you can use any user and password for your buk account following the steps described in the docs https://help.buk.cl/es/articles/3641762-como-integrarse-a-buk-a-traves-de-la-api

```ruby
require 'buk'

client = Buk::Client.new('api_endpoint', 'auth_token')

# (Optional) Set a specific Faraday connection, mainly for testing purposes
client = Buk::Client.new('api_endpoint', 'auth_token', Faraday.new)
```

### Resources

Resources this client supports:

```
https://subdominio.buk.cl/api/v1/chile/employees/active
```

### Examples

#### Employees

```ruby
# Get active employees
employees = client.active_employees

# Get employee information
employee = employees.first

# Buk::Employee model
employee.account_number              # [String] "1234567890"
employee.account_type                # [String] "Vista"
employee.active_since                # [String] "2020-07-13"
employee.active_until                # [String] nil
employee.address                     # [String] "Some Address #123"
employee.afc                         # [String] "normal"
employee.bank                        # [String] "Banco"
employee.birthday                    # [String] "1980-01-01"
employee.city                        # [String] "Santiago"
employee.civil_status                # [String] "Soltero"
employee.code_sheet                  # [String] "F1"
employee.country_code                # [String] "CL"
employee.degree                      # [String] "Ingeniero",
employee.district                    # [String] "Santiago"
employee.email                       # [String] "some.user@company.com"
employee.first_name                  # [String] "Some User"
employee.full_name                   # [String] "Some User"
employee.gender                      # [String] "M"
employee.health_company              # [String] "cruz_blanca"
employee.id                          # [String] 123
employee.location_id                 # [String] 335
employee.nationality                 # [String] "Chilena"
employee.office_phone                # [String] ""
employee.payment_method              # [String] "Transferencia Bancaria"
employee.pension_fund                # [String] "plan"
employee.pension_regime              # [String] "afp"
employee.personal_email              # [String] "user@gmail.com"
employee.phone                       # [String] "56987654321"
employee.picture_url                 # [String] "https://buk.s3.amazonaws.com/picture.png"
employee.private_role                # [String] false
employee.progressive_vacations_start # [String] "2033-07-13"
employee.region                      # [String] "RM: Metropolitana de Santiago"
employee.rut                         # [String] "12.345.678-9"
employee.second_surname              # [String] "Name"
employee.status                      # [String] "activo"
employee.surname                     # [String] "User"
employee.termination_reason          # [String] nil
employee.university                  # [String] nil
employee.custom_attributes           # [Buk::Employee::CustomAttributes]
employee.current_job                 # [Buk::Job]
employee.family_responsabilities     # [Buk::FamilyResponsability[]]
employee.custom_attributes           # [Hash] {"preferred_name": "Mr User, "drink_preference": "Wine"}

# Buk::Job model
job = employee.current_job    # [Buk::Job]
job.periodicity               # [String]  "sin_jornada"
job.frequency                 # [String]  "mensual"
job.working_schedule_type     # [String]  "exenta_art_22"
job.zone_assignment           # [String]  false
job.union                     # [String]  nil
job.id                        # [Integer] 230,
job.company_id                # [Integer] 1
job.area_id                   # [Integer] 3
job.contract_term             # [String] nil
job.contract_type             # [String] "Indefinido"
job.start_date                # [String] "2022-01-01"
job.end_date                  # [String] nil
job.contract_finishing_date_1 # [String] nil
job.contract_finishing_date_2 # [String] nil
job.weekly_hours              # [Number] 45.0
job.cost_center               # [String] ""
job.active_until              # [String] nil
job.termination_reason        # [String] nil
job.boss                      # [Hash] {"id"=>13, "rut"=>"12.345.678-9"}
job.role                      # [Buk::JobRole]
job.role.role_family          # [Buk::RoleFamily]
job.family_responsabilities   # [Buk::FamilyResponsability[]]

# Buk::JobRole model
job.role.id           # [String] 14
job.role.name         # [String] "Semi Senior Full Stack Developer"
job.role.requirements # [String] nil
job.role.role_family  # [Buk::RoleFamily]

# Buk::RoleFamily model
role_family = job.role.role_family # [Buk::RoleFamily]
role_family.id                     # [Integer] 6
role_family.name                   # [String]  "RRHH Producto"
role_family.quantity_of_roles      # [Integer] 6

# Buk::FamilyResponsability
family_responsability = employee.family_responsabilities.first
family_responsability.id                              # [Integer] 20
family_responsability.family_allowance_section        # [String] "D"
family_responsability.simple_family_responsability    # [Integer] 0
family_responsability.maternity_family_responsability # [Integer] 0
family_responsability.invalid_family_responsability   # [Integer] 0
family_responsability.start_date                      # [String] "2020-07-01"
family_responsability.end_date                        # [String] nil
family_responsability.responsability_details          # [String[]] []
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
