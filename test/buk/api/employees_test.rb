require_relative '../../test_helper'
require 'buk/models/employee'

class BukAPIEmployeesTest < Minitest::Test
  def test_that_it_has_a_active_employees_method
    assert_respond_to test_client, :active_employees
  end

  def test_it_returns_a_employee_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/api/v1/chile/employees/active') do
      stub_employee_request_single_result
    end
    
    cli = test_client(stubs)

    employees = cli.active_employees

    assert_equal employees.count, 1

    employees.each do |employee|
      assert_kind_of Buk::Employee, employee
    end
  end

  def test_it_returns_an_empty_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/api/v1/chile/employees/active') { stub_employee_request_empty }
    
    cli = test_client(stubs)
    assert_equal cli.active_employees, []
  end

  def test_it_returns_all_paginated_employees
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/api/v1/chile/employees/active?page=2') do
      stub_employee_request_multiple_result_second_page
    end
    stubs.get('/api/v1/chile/employees/active') do
      stub_employee_request_multiple_result
    end
    
    cli = test_client(stubs)

    employees = cli.active_employees

    assert_equal employees.count, 2

    employees.each do |employee|
      assert_kind_of Buk::Employee, employee
    end
  end

  private

  def stub_employee_request_empty
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"data": [], "pagination": {}}'
    ]
  end

  def stubbed_employee
    '{"person_id": 28,"id": 28,"picture_url": "https://buk.s3.amazonaws.com/picture.png","first_name": "Some User","surname": "User","second_surname": "Name","full_name": "Some User Name","rut": "12.345.678-9","nationality": "Chilena","country_code": "CL","civil_status": "Soltero","email": "some.user@company.com","personal_email": "user@gmail.com","address": "Some Address 123","city": "Santiago","district": "Santiago","location_id": 335,"region": "RM: Metropolitana de Santiago","office_phone": "","phone": "56987654321","gender": "M","birthday": "1980-01-01","university": null,"degree": "Engineer","active_since": "2020-07-13","status": "activo","payment_method": "Transferencia Bancaria","bank": "Banco","account_type": "Vista","account_number": "123456789","progressive_vacations_start": "2033-07-13","private_role": false,"code_sheet": "F1","health_company": "cruz_blanca","pension_regime": "afp","pension_fund": "planvital","afc": "normal","active_until": null,"termination_reason": null,"custom_attributes": { "preferred_name": "Mr User"},"current_job": { "periodicity": "sin_jornada", "frequency": "mensual", "working_schedule_type": "exenta_art_22", "zone_assignment": false, "union": null, "id": 230, "company_id": 1, "area_id": 3, "contract_term": null, "contract_type": "Indefinido", "start_date": "2022-01-01", "end_date": null, "contract_finishing_date_1": null, "contract_finishing_date_2": null, "weekly_hours": 45.0, "cost_center": "", "active_until": null, "termination_reason": null, "boss": { "id": 13, "rut": "17.073.858-6" }, "role": {"id": 14,"name": "Full Stack Developer","requirements": null,"role_family": { "id": 6, "name": "RRHH", "quantity_of_roles": 16} }},"family_responsabilities": [ {"id": 20,"family_allowance_section": "D","simple_family_responsability": 0,"maternity_family_responsability": 0,"invalid_family_responsability": 0,"start_date": "2020-07-01","end_date": null,"responsability_details":[]}]}'
  end
  
  def stub_employee_request_single_result
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"data": [' + stubbed_employee + '], "pagination": {}}'
    ]
  end

  def stub_employee_request_multiple_result
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"data": [' + stubbed_employee + '], "pagination": {"next":"http://domain.com/api/v1/chile/employees/active?page=2"}}'
    ]
  end
  
  def stub_employee_request_multiple_result_second_page
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"data": [' + stubbed_employee + '], "pagination": {}}'
    ]
  end
end
