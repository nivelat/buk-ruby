require "test_helper"

class BukTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Buk::VERSION
  end

  def test_the_client_returns_a_buk_client
    assert_kind_of ::Buk::Client, ::Buk.client('api_endpoint', 'auth_token')
  end
end
