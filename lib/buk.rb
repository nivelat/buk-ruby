require 'buk/version'
require 'buk/client'

module Buk
  class << self
    def client(api_endpoint, auth_token)
      Buk::Client.new(api_endpoint, auth_token)
    end
  end
end
