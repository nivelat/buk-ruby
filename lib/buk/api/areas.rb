require 'buk/models/area'

module Buk
  module API
    # Area
    #
    module Areas
      # Recibimos la información relacionada a las areas de una compañía
      #
      # @return [Hash] Localidades que cumplan con los criterios de profundidad proporcionados.
      def areas(status = nil)
        url = "/api/v1/chile/organization/areas"
        url += "?status=#{status}" unless status.nil?

        paginated_request(url).map { |a| Buk::Area.new(a) }
      end
    end
  end
end