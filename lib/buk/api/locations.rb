require 'buk/models/location'

module Buk
  module API
    # Location
    #
    module Locations
      # Recibimos la información relacionada a las localidades de un país dada
      # una profundidad.
      #
      # @return [Hash] Localidades que cumplan con los criterios de profundidad proporcionados.
      def locations(depth = 1)
        url = "/api/v1/chile/locations?depth=#{depth}"

        paginated_request(url).map { |e| Buk::Location.new(e) }
      end
    end
  end
end