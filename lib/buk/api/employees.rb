require 'buk/models/employee'

module Buk
  module API
    # Employee
    #
    module Employees
      # Retorna todos los empleados vigentes en el sistema, como parámetro
      # (opcional) recibe la fecha (en formato YYYY-MM-DD) en la que deben tener
      # un contrato vigente.
      #
      # @return [Hash] Response from API.
      def active_employees
        url = '/api/v1/chile/employees/active'

        paginated_request(url).map { |e| Buk::Employee.new(e) }
      end
    end
  end
end