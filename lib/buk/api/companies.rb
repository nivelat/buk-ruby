require 'buk/models/company'

module Buk
  module API
    # Company
    #
    module Companies
      # Recibimos la información relacionada a las localidades de un país dada
      # una profundidad.
      #
      # @return [Hash] Localidades que cumplan con los criterios de profundidad proporcionados.
      def companies
        url = '/api/v1/chile/companies'

        paginated_request(url).map { |e| Buk::Company.new(e) }
      end
    end
  end
end