module Buk
  module Services
    class Organization
      attr_accessor :areas

      def initialize(areas)
        @areas = areas || []
      end

      def level1
        @areas.select { |a| a.parent_area.nil? }
      end

      def level2
        (areas_level1 || []).map do |a|
          a.children_area || []
        end.flatten
      end

      def level3
        (areas_level2 || []).map do |a|
          a.children_area || []
        end.flatten
      end

      def level4
        (areas_level3 || []).map do |a|
          a.children_area || []
        end.flatten
      end

      def level5
        (areas_level4 || []).map do |a|
          a.children_area || []
        end.flatten
      end

      def directory
        area_index = {}
        areas.each do |area|
          area_index[area.id] = area
        end

        area_directory = {}

        level1.each do |area1|
          area_directory[area1.id] = {
            "level1": area1,
            "level2": nil,
            "level3": nil,
            "level4": nil,
            "level5": nil
          }

          area1.children_area.each do |a2|
            area2 = area_index[a2['id']]
            area_directory[area2.id] = {
              "level1": area1,
              "level2": area2,
              "level3": nil,
              "level4": nil,
              "level5": nil
            }

            area2.children_area.each do |a3|
              area3 = area_index[a3['id']]
              area_directory[area3.id] = {
                "level1": area1,
                "level2": area2,
                "level3": area3,
                "level4": nil,
                "level5": nil
              }

              area3.children_area.each do |a4|
                area4 = area_index[a4['id']]
                area_directory[area4.id] = {
                  "level1": area1,
                  "level2": area2,
                  "level3": area3,
                  "level4": area4,
                  "level5": nil
                }

                area4.children_area.each do |a5|
                  area5 = area_index[a5['id']]
                  area_directory[area5.id] = {
                    "level1": area1,
                    "level2": area2,
                    "level3": area3,
                    "level4": area4,
                    "level5": area5
                  }
                end
              end
            end
          end
        end

        area_directory
      end
    end
  end
end