module Buk
  class Company
    attr_accessor :custom_attributes,
                  :id,
                  :legal_agents,
                  :name,
                  :rut

    def initialize(attributes)
      @custom_attributes = attributes['custom_attributes']
      @id = attributes['id']
      @legal_agents = attributes['legal_agents']
      @name = attributes['name']
      @rut = attributes['rut']
    end
  end
end
