require 'buk/models/job_role'

module Buk
  class Job
    attr_accessor :active_until,
                  :area_id,
                  :boss,
                  :company_id,
                  :contract_finishing_date_1,
                  :contract_finishing_date_2,
                  :contract_term,
                  :contract_type,
                  :cost_center,
                  :end_date,
                  :family_responsabilities,
                  :frequency,
                  :id,
                  :level1,
                  :level2,
                  :level3,
                  :level4,
                  :level5,
                  :periodicity,
                  :role,
                  :start_date,
                  :termination_reason,
                  :union,
                  :weekly_hours,
                  :working_schedule_type,
                  :zone_assignment

    def initialize(attributes)
      @active_until = attributes['active_until']
      @area_id = attributes['area_id']
      @boss = attributes['boss']
      @company_id = attributes['company_id']
      @contract_finishing_date_1 = attributes['contract_finishing_date_1']
      @contract_finishing_date_2 = attributes['contract_finishing_date_2']
      @contract_term = attributes['contract_term']
      @contract_type = attributes['contract_type']
      @cost_center = attributes['cost_center']
      @end_date = attributes['end_date']
      @family_responsabilities = attributes['family_responsabilities']
      @frequency = attributes['frequency']
      @id = attributes['id']
      @periodicity = attributes['periodicity']
      @start_date = attributes['start_date']
      @termination_reason = attributes['termination_reason']
      @union = attributes['union']
      @weekly_hours = attributes['weekly_hours']
      @working_schedule_type = attributes['working_schedule_type']
      @zone_assignment = attributes['zone_assignment']

      unless attributes['role'].nil?
        @role = Buk::JobRole.new(attributes['role'])
      end
    end
  end
end
