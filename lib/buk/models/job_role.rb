require 'buk/models/role_family'

module Buk
  class JobRole
    attr_accessor :id,
                  :name,
                  :requirements,
                  :role_family

    def initialize(attributes)
      @id = attributes['id']
      @name = attributes['name']
      @requirements = attributes['requirements']

      unless attributes['role_family'].nil?
        @role_family = Buk::RoleFamily.new(attributes['role_family'])
      end
    end
  end
end
