module Buk
  class RoleFamily
    attr_accessor :id,
                  :name,
                  :quantity_of_roles

    def initialize(attributes)
      @id = attributes['id']
      @name = attributes['name']
      @quantity_of_roles = attributes['quantity_of_roles']
    end
  end
end
