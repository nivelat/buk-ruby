module Buk
  class Area
    attr_accessor :address,
                  :children_area,
                  :city,
                  :cost_center,
                  :custom_attributes,
                  :depth,
                  :id,
                  :name,
                  :parent_area,
                  :status

    def initialize(attributes)
      @address = attributes['address']
      @children_area = attributes['children_area']
      @city = attributes['city']
      @cost_center = attributes['cost_center']
      @custom_attributes = attributes['custom_attributes']
      @depth = attributes['depth']
      @id = attributes['id']
      @name = attributes['name']
      @parent_area = attributes['parent_area']
      @status = attributes['status']
    end
  end
end
