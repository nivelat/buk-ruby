module Buk
  class Location
    attr_accessor :depth,
                  :id,
                  :name

    def initialize(attributes)
      @depth = attributes['depth']
      @id = attributes['id']
      @name = attributes['name']
    end
  end
end
