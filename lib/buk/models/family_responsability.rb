module Buk
  class FamilyResponsability
    attr_accessor :end_date,
                  :family_allowance_section,
                  :id,
                  :invalid_family_responsability,
                  :maternity_family_responsability,
                  :responsability_details,
                  :simple_family_responsability,
                  :start_date

    def initialize(attributes)
      @end_date = attributes['end_date']
      @family_allowance_section = attributes['family_allowance_section']
      @id = attributes['id']
      @invalid_family_responsability = attributes['invalid_family_responsability']
      @maternity_family_responsability = attributes['maternity_family_responsability']
      @responsability_details = attributes['responsability_details']
      @simple_family_responsability = attributes['simple_family_responsability']
      @start_date = attributes['start_date']
    end
  end
end
