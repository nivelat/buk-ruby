require 'buk/models/job'
require 'buk/models/family_responsability'

module Buk
  class Employee
    attr_accessor :account_number,
                  :account_type,
                  :active_since,
                  :active_until,
                  :address,
                  :afc,
                  :bank,
                  :birthday,
                  :city,
                  :civil_status,
                  :code_sheet,
                  :country_code,
                  :current_job,
                  :custom_attributes,
                  :degree,
                  :district,
                  :email,
                  :family_responsabilities,
                  :first_name,
                  :full_name,
                  :gender,
                  :health_company,
                  :id,
                  :location_id,
                  :nationality,
                  :office_phone,
                  :payment_method,
                  :pension_fund,
                  :pension_regime,
                  :person_id,
                  :personal_email,
                  :phone,
                  :picture_url,
                  :private_role,
                  :progressive_vacations_start,
                  :region,
                  :rut,
                  :second_surname,
                  :status,
                  :surname,
                  :termination_reason,
                  :university

    def initialize(attributes)
      @account_number = attributes['account_number']
      @account_type = attributes['account_type']
      @active_since = attributes['active_since']
      @active_until = attributes['active_until']
      @address = attributes['address']
      @afc = attributes['afc']
      @bank = attributes['bank']
      @birthday = attributes['birthday']
      @city = attributes['city']
      @civil_status = attributes['civil_status']
      @code_sheet = attributes['code_sheet']
      @country_code = attributes['country_code']
      @current_job = attributes['current_job']
      @custom_attributes = attributes['custom_attributes']
      @degree = attributes['degree']
      @district = attributes['district']
      @email = attributes['email']
      @family_responsabilities = attributes['family_responsabilities']
      @first_name = attributes['first_name']
      @full_name = attributes['full_name']
      @gender = attributes['gender']
      @health_company = attributes['health_company']
      @id = attributes['id']
      @location_id = attributes['location_id']
      @nationality = attributes['nationality']
      @office_phone = attributes['office_phone']
      @payment_method = attributes['payment_method']
      @pension_fund = attributes['pension_fund']
      @pension_regime = attributes['pension_regime']
      @person_id = attributes['person_id']
      @personal_email = attributes['personal_email']
      @phone = attributes['phone']
      @picture_url = attributes['picture_url']
      @private_role = attributes['private_role']
      @progressive_vacations_start = attributes['progressive_vacations_start']
      @region = attributes['region']
      @rut = attributes['rut']
      @second_surname = attributes['second_surname']
      @status = attributes['status']
      @surname = attributes['surname']
      @termination_reason = attributes['termination_reason']
      @university = attributes['university']

      unless attributes['current_job'].nil?
        @current_job = Buk::Job.new(attributes['current_job'])
      end

      unless attributes['family_responsabilities'].nil?
        responsabilities = attributes['family_responsabilities'] || []
        @family_responsabilities = responsabilities.map do |fr|
          Buk::FamilyResponsability.new(fr)
        end
      end
    end
  end
end
