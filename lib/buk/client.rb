require 'faraday'

require 'buk/api/areas'
require 'buk/api/companies'
require 'buk/api/employees'
require 'buk/api/locations'

module Buk
  class Client
    include Buk::API::Areas
    include Buk::API::Companies
    include Buk::API::Employees
    include Buk::API::Locations

    attr_reader :auth_token

    def initialize(api_endpoint, auth_token, conn = nil)
      @api_endpoint = api_endpoint
      @auth_token = auth_token
      @conn = conn
    end

    private

    def request(method, path, body = nil)
      client.public_send(method, path, body).body
    end

    def paginated_request(url, _params = {})
      result = []

      while !url.nil?
        response = client.get(url)

        if response.status != 200
          raise StandardError, "Code: #{response.status}, response: #{response.body}"
        end

        parsed_response = response.body

        result += parsed_response['data']

        url = parsed_response.dig('pagination', 'next')
      end

      result
    end

    def client
      @_client = @conn || @_client || ::Faraday.new(@api_endpoint) do |connection|
        connection.headers['auth_token'] = @auth_token unless @auth_token.nil?
        connection.headers['Accept'] = 'application/json'

        connection.request :json
        connection.request :url_encoded

        connection.response :json
        connection.response :logger, nil, { headers: true, bodies: true }

        connection.adapter Faraday.default_adapter
      end
    end
  end
end
